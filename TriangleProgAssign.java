/*
 * TriangleProgAssign.java
 * Megan Coyne, coynem6
 */

/*
 * Name: Megan Coyne
 * User Name: coynem6
 * Assignment: Programming Assignment 2
 * 
 * Description: This program takes in 3 x and y coordinates from the user
 * and outputs the triangle's side lengths, angle measurements, area, and perimeter.
 */

import java.util.Scanner;

public class TriangleProgAssign {

	//this method gets each of the coordinates for the triangle corners from the user
	public static int getCoordinate(String promtUser)
	{
		Scanner in = new Scanner(System.in);
		System.out.print(promtUser);
		int x = in.nextInt();
		//this if statement tells the user there is an error and ends the program if there is a negative coordinate value inputed
		if (x < 0) {
			System.err.println("\nInvalid input; terminating program");
			System.exit(0);
		} 
		return x;
	}
	
	//this method calculates a side length of the triangle
	public static double calcSideLength(int x1, int y1, int x2, int y2)
	{
		double sideLength = Math.sqrt(((x2-x1)*(x2-x1))+((y2-y1)*(y2-y1)));
		//this if statement tells the user there is an error and ends the program if there is a negative coordinate value
		if (x1 < 0 || x2 < 0 || y1 <0 || y2 <0) {
			System.err.println("\nInvalid input; terminating program");
			System.exit(0);
		}
		return sideLength;
	}
	
	//this method calculates the alpha angle measurement of the triangle and returns it in degrees
	public static double calcAlphaAngle(double a, double b, double c)
	{
		double alphaAngle = Math.acos(((b*b+c*c)-(a*a))/(2*b*c));
		alphaAngle = Math.toDegrees(alphaAngle);
		//this if statement tells the user there is an error and ends the program if there is a negative side length a, b, or c
		if (a < 0 || b < 0 || c < 0) {
			System.err.println("\nInvalid input; terminating program");
			System.exit(0);
		}
		return alphaAngle;
	}
	
	//this method calculates the beta angle measurement of the triangle and returns it in degrees
	public static double calcBetaAngle(double a, double b, double c)
	{
		double betaAngle = Math.acos(((a*a+c*c)-(b*b))/(2*a*c));
		betaAngle = Math.toDegrees(betaAngle);
		//this if statement tells the user there is an error and ends the program if there is a negative side length a, b, or c
		if (a < 0 || b < 0 || c < 0) {
			System.err.println("\nInvalid input; terminating program");
			System.exit(0);
		}
		return betaAngle;
	}
	
	//this method calculates the gamma angle measurement of the triangle and returns it in degrees
	public static double calcGammaAngle(double a, double b, double c)
	{
		double gammaAngle = Math.acos(((a*a+b*b)-(c*c))/(2*a*b));
		gammaAngle = Math.toDegrees(gammaAngle);
		//this if statement tells the user there is an error and ends the program if there is a negative side length a, b, or c
		if (a < 0 || b < 0 || c < 0) {
			System.err.println("\nInvalid input; terminating program");
			System.exit(0);
		}
		return gammaAngle;
	}
	
	//this method calculates the perimeter of the triangle
	public static double calcPerimeter(double side1, double side2, double side3)
	{
		double perimeter = side1 + side2 + side3;
		//this if statement tells the user there is an error and ends the program if there is a negative side length side1, side2, or side3
		if (side1 < 0 || side2 < 0 || side3 < 0) {
			System.err.println("\nInvalid input; terminating program");
			System.exit(0);
		}
		return perimeter;
	}
	
	//this method calculates the area of the triangle
	public static double calcArea(double a, double b, double c)
	{
		double s = (a+b+c)/2;  //the sempiperimeter
		double area = Math.sqrt(s*(s-a)*(s-b)*(s-c));
		//this if statement tells the user there is an error and ends the program if there is a negative side length a, b, or c
		if (a < 0 || b < 0 || c < 0) {
			System.err.println("\nInvalid input; terminating program");
			System.exit(0);
		}
		return area;
	}
	
	public static void main(String[] args) {

		System.out.println("This program calculates the side lengths, angles, perimeter and area\n"
				+ "of a triangle, given the x- and y-coordinates of its corner points. \n");

		//Getting the coordinates of the triangle
		int x1 = getCoordinate("Enter the x-coordinate of the first corner point: ");
		int y1 = getCoordinate("Enter the y-coordinate of the first corner point: ");
		int x2 = getCoordinate("Enter the x-coordinate of the second corner point: ");
		int y2 = getCoordinate("Enter the y-coordinate of the second corner point: ");
		int x3 = getCoordinate("Enter the x-coordinate of the third corner point: ");
		int y3 = getCoordinate("Enter the y-coordinate of the third corner point: ");
		
		//Calculating the side lengths of the triangle
		double sideLength1 = calcSideLength(x1,y1,x2,y2); 
		double sideLength2 = calcSideLength(x2,y2,x3,y3);
		double sideLength3 = calcSideLength(x1,y1,x3,y3);

		//Calculating the angles of the triangle
		double alphaAngle = calcAlphaAngle(sideLength1, sideLength2, sideLength3);
		double betaAngle = calcBetaAngle(sideLength1, sideLength2, sideLength3);
		double gammaAngle = calcGammaAngle(sideLength1, sideLength2, sideLength3);

		//Calculating the perimeter and area of the triangle
		double perimeter = calcPerimeter(sideLength1, sideLength2, sideLength3);
		double area = calcArea(sideLength1, sideLength2, sideLength3);
		
		//Outputting the calculated values to the user
		System.out.printf("\nThe lengths of the three sides of the triangle are %.1f, %.1f and %.1f", sideLength1, sideLength2, sideLength3);
		System.out.printf("\n\nThe three angles of the triangle are %.1f degrees, %.1f degrees and %.1f degrees", alphaAngle, betaAngle, gammaAngle);
		System.out.printf("\n\nThe perimeter of the triangle is %.1f, and the area of the triangle is %.1f",perimeter, area);

		//end of program
	}

} 
